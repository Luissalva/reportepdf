
package Conec;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Usuario
 */
public class SQL {
    private static SQL instance;
    
    private Connection cnn;
    Statement st;
//    private String url = "jdbc:sqlserver://localhost:4065/Empresa";
//    private String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
//    private String usuario = "luis";
//    private String pass = "luis123";

    public SQL() {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                       cnn = DriverManager.getConnection("jdbc:sqlserver://localhost:4065;databaseName=Empresa","luis","luis123");
                       System.out.println("Conectado Con Exito");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Conectado" + ex);
        }

    }

    public boolean ejecutar(String query){
        try {
            st = cnn.createStatement();
            st.execute(query);
            //desconectar();
            return true;
        } catch (Exception e) {
            
        }
        return false;
    }
    
    public ResultSet consultar(String query){
        try{
            st = cnn.createStatement();
            return st.executeQuery(query);
        }catch(SQLException e){
            return null;
        }
    }
    
    
    public synchronized static SQL conectar() {

        if (instance == null) {
            instance = new SQL();
        }
        return instance;

    }

    public Connection getCnn() {
        return cnn;
    }

    public void cerrarConexion() {
        instance = null;
    }

   
}
