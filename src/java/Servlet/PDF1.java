
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Usuario
 */
public class PDF1 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException, JRException {
        response.setContentType("text/html;charset=UTF-8");
        
     //   int rol = Integer.parseInt(request.getParameter("rol"));
       
        Connection con ;
      Class.forName("org.postgresql.Driver").newInstance();
               con = (Connection) DriverManager.getConnection("jdbc:postgresql://aymasesoresperu.dyndns.org:5432/INVOICE","postgres","123456");
              Map parameters = new HashMap();
            
               //parameters.put("Cod_Voucher",rol);
//               parameters.put("TipoMoneda",moneda);
               ServletOutputStream out;
               //C:\Users\Usuario\Documents\NetBeansProjects\ReportePDF\web
               
               JasperReport jasperReportjasperReport ;
               jasperReportjasperReport = (JasperReport) JRLoader.loadObject("C:/Users/Usuario/Documents/NetBeansProjects/ReporPDF/web/FacturaElec.jasper");
               //byte[] fichero = JasperRunManager.runReportToPdf ("FacturaElec.jasper", parameters, con);
               //C:\Users\Usuario\Documents\NetBeansProjects\ReportePDF\web
               // jasperReportjasperReport = (JasperReport) JRLoader.loadObject("ReportePDF/web/FacturaElec.jasper");
               byte[] fichero = JasperRunManager.runReportToPdf ("C:/Users/Usuario/Documents/NetBeansProjects/ReporPDF/web/FacturaElec.jasper", parameters, con);
//               jasperReportjasperReport = (JasperReport) JRLoader.loadObject("C:/Users/Usuario/Documents/NetBeansProjects/ReportesPDF/web/report1.jasper");
//               byte[] fichero = JasperRunManager.runReportToPdf ("C:/Users/Usuario/Documents/NetBeansProjects/ReportesPDF/web/report1.jasper", parameters, con);

// Y enviamos el pdf a la salida del navegador como podríamos hacer con cualquier otro pdf
response.setContentType ("application/pdf");
response.setHeader ("Content-disposition", "inline; filename=Reporte.pdf");
response.setHeader ("Cache-Control", "max-age=50");
response.setHeader ("Pragma", "No-cache");
response.setDateHeader ("Expires", 0);
response.setContentLength (fichero.length);
out = response.getOutputStream ();

out.write (fichero, 0, fichero.length);
out.flush ();
out.close ();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(PDF1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
